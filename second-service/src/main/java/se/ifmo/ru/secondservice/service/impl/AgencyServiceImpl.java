package se.ifmo.ru.secondservice.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se.ifmo.ru.secondservice.external.client.CatalogRestClient;
import se.ifmo.ru.secondservice.mapper.FlatMapper;
import se.ifmo.ru.secondservice.service.api.AgencyService;
import se.ifmo.ru.secondservice.service.model.Flat;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

@Service
public class AgencyServiceImpl implements AgencyService {

    private final  CatalogRestClient catalogRestClient;
    private final FlatMapper flatMapper;

    @Autowired
    public AgencyServiceImpl(CatalogRestClient catalogRestClient, FlatMapper flatMapper){
        this.catalogRestClient = catalogRestClient;
        this.flatMapper = flatMapper;
    }

    @Override
    public Flat findFlatWithBalcony(boolean cheapest, boolean balcony) throws ClassNotFoundException {

        List<Flat> flats = flatMapper.fromRestClient(catalogRestClient.getAllFlats());

        if (flats == null || flats.isEmpty()) {
            throw new ClassNotFoundException("Not found list of flats!");
        }

        Comparator<Flat> priceComparator = Comparator.comparingDouble(Flat::getPrice);
        if (!cheapest) {
            priceComparator = priceComparator.reversed();
        }

        return flats.stream()
                .filter(flat -> flat.getBalcony() == balcony)
                .min(priceComparator)
                .orElseThrow(() -> new ClassCastException("Not found suitable flat!"));
    }

    @Override
    public long getMostExpensiveFlat(long id1, long id2, long id3) throws ClassNotFoundException {

        Flat flat1 = flatMapper.fromRestClient(catalogRestClient.getFlatById(id1));
        Flat flat2 = flatMapper.fromRestClient(catalogRestClient.getFlatById(id2));
        Flat flat3 = flatMapper.fromRestClient(catalogRestClient.getFlatById(id3));

        return Stream.of(flat1, flat2, flat3)
                .filter(Objects::nonNull)
                .max(Comparator.comparingDouble(Flat::getPrice))
                .orElseThrow(() -> new ClassNotFoundException("One or more flats not found"))
                .getId();
        }
}
