package se.ifmo.ru.firstservice.web.model.response;

import lombok.*;

import java.util.List;

@Getter
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UniqueViewDto {
    private List<String> uniqueView;

}
